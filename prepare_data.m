clc
clear
close all
%%
admissions_data = readtable('data/admissions_data.csv');
disease_data = readtable('data/disease_data.csv');
labs_data = readtable('data/labs_data.csv');
prescriptions_data = readtable('data/prescriptions_data.csv');
patients_data = readtable('data/patients_data.csv');
train_labels = readtable('data/train_labels.csv');
sample_submission = readtable('data/sample_submission.csv');
%% Собираем два массива данных по пациантам - для обучения данные 2018 года, для основного прогноза - данные 2019 года

load icd10_codes.mat
members = patients_data.member_id;
input_data = patients_data;

% Для удобства последовательность пациентов отсортирована в алф. порядке
[members, sort_index] = sort(members);
input_data = input_data(sort_index,:);
input_data_2019 = input_data;
input_data_2018 = input_data;

% Год назад пациенты были на год младше
input_data_2018.patient_age = input_data_2018.patient_age-1;
%%
% Выходная последовательность (новые болезни 2019) отсортирована так же
output_data = train_labels;
members = output_data.member_id;
[members, sort_index] = sort(members);
output_data = output_data(sort_index,:);
save ('output_data','output_data')
%% Уменьшение размерности данных об анализах часть 1
% Оставим только те анализы, которые встречаются наиболее часто
% Для этого посмотрим распределение
[freq,names] = histcounts(categorical(labs_data.performed_test_name));
[sort_freq,sort_index] = sort(freq,'descend');
sort_names = names(sort_index);

% figure;plot(sort_freq);
% xlim([1,20])

% Оставим только 5 наиболее часто встречающихся анализов
N_labs = 5;
labs_names_for_input_data = sort_names(1:N_labs);

% Добавляем в таблицы предикторов новые колонки, пока пустые
add_table = array2table(zeros(size(input_data,1),length(labs_names_for_input_data)));
add_table.Properties.VariableNames = labs_names_for_input_data;
last_filled_column = size(input_data_2018,2);
input_data_2018 = [input_data_2018,add_table];
input_data_2019 = [input_data_2019,add_table];


%% Уменьшение размерности данных об анализах часть 2
% Будем учитывать только те анализы, где результаты выходят из нормы
% Подготовим индексы для ускоренного анализа таблицы
labs_data_member_id = labs_data.member_id;
labs_data_performed_test_name = labs_data.performed_test_name;
labs_data_abnormal_code = labs_data.abnormal_code;


interesting_rows=false(size(labs_data_member_id));
for i = 1:length(labs_names_for_input_data)
    lab = labs_names_for_input_data{i};
    labels{i} = strcmp(labs_data_performed_test_name, lab);
    interesting_rows=interesting_rows|labels{i} ;
end
abnormal = ~strcmp(labs_data_abnormal_code, '');


[~,member_index] = ismember(labs_data_member_id,members);

% for i = 1:length(members)
%     memb_ind{i} = find(member_index==i);
% end
load memb_ind

date_of_service = datenum(labs_data.date_of_service);
end_time_for_train = datenum('01-Jan-2019');
end_time_for_test = datenum('01-Jan-2020');

%% Определение функции влияния времени на результат
% Предположим, что чем более давним является событие (анализ,
% госпитализация, прием лкарства), тем меньше оно влияет на вероятность
% болезни в следующем году. Например, влияени будет убывать по кубическому
% закону от единцы (если событие было 31 декабря) до 0,5 (если событие было
% 1 января).
time_influence = @(x) (0.5+((366-x)/365).^3/2);
% figure; plot(time_influence(1:365));
%% Уменьшение размерности данных об анализах часть 3

% Зная измеренное значение и границы диапазонов, определим важность анализа
% как отклонение от одной из границ, деленное на центральное значение. Знак
% соответсвует типу отклонения: Low или High. И умножаем результат на
% функцию влияения времени. Процедура выполняется и дял обучающей, и для
% тестовой выборок.
% Если одинаковых анализов было несколько, берем только последний.
try
    load prepared_data_part_1.mat
catch

    for i = 1:length(members)
        memb = members{i};
        mins = false(size(member_index));
        mins(memb_ind{i})=true;
        if any(mins & abnormal & interesting_rows)

            % train
            prior = zeros(1,length(labs_names_for_input_data));
            for j = 1:length(labs_names_for_input_data)
                lab = labs_names_for_input_data{j};
                labs = labs_data(mins & labels{j} & abnormal & date_of_service<end_time_for_train,:);
                if ~isempty(labs)
                    last = find(labs.date_of_service==max(labs.date_of_service),1,'last');
                    min_val = labs.normal_low_value_numeric(last);
                    max_val = labs.normal_high_value_numeric(last);
                    result = labs.result_value(last)/1000;
                    days_before_newyear = end_time_for_train - datenum(labs.date_of_service(last));
                    prior(j) = max(min_val-result, result-max_val)/mean([max_val,min_val]) * time_influence(days_before_newyear) * sign(result-max_val);
                end
            end
            if sum(prior~=0)
                input_data_2018{i,last_filled_column+1:last_filled_column+length(labs_names_for_input_data)} = prior;
            end

            % test
            prior = zeros(1,length(labs_names_for_input_data));
            for j = 1:length(labs_names_for_input_data)
                lab = labs_names_for_input_data{j};
                labs = labs_data(mins & labels{j} & abnormal & date_of_service<end_time_for_test & date_of_service>=end_time_for_train,:);
                if ~isempty(labs)
                    last = find(labs.date_of_service==max(labs.date_of_service),1,'last');
                    min_val = labs.normal_low_value_numeric(last);
                    max_val = labs.normal_high_value_numeric(last);
                    result = labs.result_value(last)/1000;
                    days_before_newyear = end_time_for_test - datenum(labs.date_of_service(last));
                    prior(j) = max(min_val-result, result-max_val)/mean([max_val,min_val]) * time_influence(days_before_newyear) * sign(result-max_val);
                end
            end
            if sum(prior~=0)
                input_data_2019{i,last_filled_column+1:last_filled_column+length(labs_names_for_input_data)} = prior;
            end
        end
    end

    save('prepared_data_part_1.mat', 'input_data_2018', 'input_data_2019')
end

%% Уменьшение размерности данных о лекарствах часть 1
% Оставим только те лекарства, которые встречаются наиболее часто
% Для этого посмотрим распределение
try
    load drugs_1w.mat
catch
    for i = 1:size(prescriptions_data,1)
        words = split(prescriptions_data.drug_name{i},' ');
        drugs{i} = words{1};
    end
end
[freq,names] = histcounts(categorical(drugs));
[sort_freq,sort_index] = sort(freq,'descend');
sort_names = names(sort_index);

% figure;plot(sort_freq);
% xlim([1,20])

% Оставим 12 наиболее часто встречающихся лекарств
N_drugs = 12;

drugs_names_for_input_data = sort_names(1:N_drugs);

add_table = array2table(zeros(size(input_data,1),length(drugs_names_for_input_data)));
add_table.Properties.VariableNames = drugs_names_for_input_data;

last_filled_column = size(input_data_2018,2);
input_data_2018 = [input_data_2018,add_table];
input_data_2019 = [input_data_2019,add_table];


%% Уменьшение размерности данных о лекарствах часть 2
% Подготовим индексы для ускоренного анализа таблицы
prescriptions_data_member_id = prescriptions_data.member_id;
prescriptions_data_drug_name = drugs';


interesting_rows=false(size(prescriptions_data_member_id));
for i = 1:length(drugs_names_for_input_data)
    drug = drugs_names_for_input_data{i};
    labels{i} = strcmp(prescriptions_data_drug_name, drug);
    interesting_rows=interesting_rows|labels{i} ;
end

[~,member_index] = ismember(prescriptions_data_member_id,members);

% memb_ind={};
% for i = 1:length(members)
%     memb_ind{i} = find(member_index==i);
% end
load memb_ind_d

date_filled = datenum(prescriptions_data.date_filled);
end_time_for_train = datenum('01-Jan-2019');
end_time_for_test = datenum('01-Jan-2020');


%% Уменьшение размерности данных о лекарствах часть 3
% Здесь, в отличие от анализов, анализируем не только последний прием
% лекарства, а все в году, но с учетом влияния времени.
% Основной характеристикой считаем количество вещества, а не длительность приёма
try
    load prepared_data_part_2
catch

    for i = 1:length(members)
        memb = members{i};
        mins = false(size(member_index));
        mins(memb_ind{i})=true;
        if any(mins & interesting_rows)

            % train
            prior = zeros(1,length(drugs_names_for_input_data));
            for j = 1:length(drugs_names_for_input_data)
                drug = drugs_names_for_input_data{j};
                drugs = prescriptions_data(mins & labels{j}  & date_filled<end_time_for_train,:);
                if ~isempty(drugs)
                    last = find(drugs.date_filled==max(drugs.date_filled),1,'last');
                    days_before_newyear = end_time_for_train - datenum(drugs.date_filled(last));
                    prior(j) = drugs.metric_quantity(last) * time_influence(days_before_newyear);
                end
            end
            if sum(prior~=0)
                input_data_2018{i,last_filled_column+1:last_filled_column+length(drugs_names_for_input_data)} = prior;
            end

            % test
            prior = zeros(1,length(drugs_names_for_input_data));
            for j = 1:length(drugs_names_for_input_data)
                drug = drugs_names_for_input_data{j};
                drugs = prescriptions_data(mins & labels{j} & date_filled<end_time_for_test & date_filled>=end_time_for_train,:);
                if ~isempty(drugs)
                    last = find(drugs.date_filled==max(drugs.date_filled),1,'last');

                    days_before_newyear = end_time_for_test - datenum(drugs.date_filled(last));
                    prior(j) = drugs.metric_quantity(last) * time_influence(days_before_newyear) ;
                end
            end
            if sum(prior~=0)
                input_data_2019{i,last_filled_column+1:last_filled_column+length(drugs_names_for_input_data)} = prior;
            end

        end
    end

    save('prepared_data_part_2.mat', 'input_data_2018', 'input_data_2019')
end

%% Добавляем в тсходные таблицы колонки для icd10 кодов из данных о госпитализации
% Так как таких кодов несколько тысяч, необходимо сократить их окличество,
% объединив в группы. Самый логичный способ объединить болезни - по группе
% органов, или если точнее, по самой классификации болезней ICD10. Более
% подробная информация - внутри кода функции icd10_decode.m

adm_icd_codes = admissions_data.icd_code;
try
    load add_adm
catch
    for i = 1:size(adm_icd_codes,1)
        chapter(i,:) = icd10_decode(adm_icd_codes{i});
    end
end

% Добавляется 22 колонки групп болезней, с которыми была связана
% госпитализация.
admissions_data = [admissions_data,array2table(chapter)];

%% Госпитализация
try
    load prepared_data_part_3
catch
    % Если человек за год болел болезнями одной и той же группы N
    % раз, в соответсвующей колонке будет стоять число N.
    % Влияние времени для каждой госпитализации также учитваем.
    for i = 1:length(members)
        memb = members{i};
        cmp = strcmp(admissions_data.member_id, memb);
        adms = admissions_data(cmp & admissions_data.discharge_date<datetime(datestr(end_time_for_train)),:);
        if ~isempty(adms)
            days_before_newyear = end_time_for_train - datenum(adms.discharge_date);
            coeff = time_influence(days_before_newyear);
            coeffs = repmat(coeff,[1,size(codes,1)]);
            adm_chapter(i,:) = sum(adms{:,end-size(codes,1)+1:end}.*coeffs,1);
        else
            adm_chapter(i,:) = zeros(1,size(codes,1));
        end

    end
    input_data_2018 = [input_data_2018,array2table(adm_chapter)];

    for i = 1:length(members)
        memb = members{i};
        cmp = strcmp(admissions_data.member_id, memb);
        adms = admissions_data(cmp & admissions_data.discharge_date<datetime(datestr(end_time_for_test))  & admissions_data.discharge_date>=datetime(datestr(end_time_for_train)) ,:);
        if ~isempty(adms)
            days_before_newyear = end_time_for_test - datenum(adms.discharge_date);
            coeff = time_influence(days_before_newyear);
            coeffs = repmat(coeff,[1,size(codes,1)]);
            adm_chapter(i,:) = sum(adms{:,end-size(codes,1)+1:end}.*coeffs,1);
        else
            adm_chapter(i,:) = zeros(1,size(codes,1));
        end

    end
    input_data_2019 = [input_data_2019,array2table(adm_chapter)];

    save('prepared_data_part_3.mat', 'input_data_2018', 'input_data_2019')
end

%% Добавляем в исходные таблицы колонки для icd10 и hcc кодов из данных о болезнях

dis_icd_codes = disease_data.icd_code;
try
    load add_dis
catch
    chapter=[];
    for i = 1:size(dis_icd_codes,1)
        chapter(i,:) = icd10_decode(dis_icd_codes{i});
    end
    disease_data = [disease_data,array2table(chapter)];
end


%% Болезни
try
    load prepared_data_part_4.mat
catch

    hcc = sample_submission.Properties.VariableNames(2:end);
    for i = 1:length(hcc)
        hcc_num(i) = str2double(hcc{i}(5:end));
    end

    for i = 1:length(members)
        % Время болезни не известно, поэтому просто сумируем количество
        % болезней из группы

        % Оставляем данные только о тех 29 hcc кодах, которые нужно прогнозировать
        memb = members{i};
        cmp = strcmp(disease_data.member_id, memb);
        diss = disease_data(cmp & disease_data.year_of_service==2018,:);
        if ~isempty(diss)
            for j = 1:length(hcc)
                disease_hcc(i,j) = sum(diss.hcc_code == hcc_num(j));
            end
            disease_icd(i,:) = sum(diss{:,end-size(codes,1)+1:end});
        else
            disease_hcc(i,:) = zeros(1,length(hcc));
            disease_icd(i,:) = zeros(1,size(codes,1));
        end
    end
    input_data_2018 = [input_data_2018,array2table(disease_hcc),array2table(disease_icd)];


    for i = 1:length(members)
        memb = members{i};
        cmp = strcmp(disease_data.member_id, memb);
        diss = disease_data(cmp & disease_data.year_of_service==2019,:);
        if ~isempty(diss)
            for j = 1:length(hcc)
                disease_hcc(i,j) = sum(diss.hcc_code == hcc_num(j));
            end
            disease_icd(i,:) = sum(diss{:,end-size(codes,1)+1:end});
        else
            disease_hcc(i,:) = zeros(1,length(hcc));
            disease_icd(i,:) = zeros(1,size(codes,1));
        end
    end
    input_data_2019 = [input_data_2019,array2table(disease_hcc),array2table(disease_icd)];

    save('prepared_data_part_4.mat', 'input_data_2018', 'input_data_2019')
end
%% Результат
% Таким образом, полученные таблицы  входных данных (input_data_2018 и input_data_2019) имеют 100 столбцов:
% Столбцы 1-9 - исходные данные из таблицы patients_data
% Столбцы 10-14 - информация об отклонениях от нормы в 5 наиболее частых анализах (из labs_data)
% Столбцы 15-26 - информация о приеме лекарств (из prescriptions_data)
% Столбцы 27-48 - информация о болезнях, связанных с госпитализацией (из admissions_data)
% Столбцы 49-78 - информация о hcc кодах болезней (из disease_data)
% Столбцы 79-100 - информация о icd10 групах болезней (из disease_data)







