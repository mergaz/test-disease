function chapter = icd10_decode_one(codes_string)
% Функция определения группы заболеваний по её коду
% Информация взята из https://en.wikipedia.org/wiki/ICD-10
% На входе - одна строка, на выходе - один вектор из 0 и 1, определяющий
% принадлежность к группам 1-22

load icd10_codes.mat codes
alf = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
ch=zeros(1,size(codes,1));

cs = {codes_string};

for ics = 1:length(cs)
    icd10 = cs{ics};

    if length(icd10)>=3

        letter_icd10 = icd10(1);
        number_icd10 = icd10(2:3);
        code = find(alf==letter_icd10) * 1000 + str2double(number_icd10);
        if isempty(code)
            code = 0;
        end


        for i = 1:size(codes,1)
            letter_start = codes{i,1}(1);
            number_start = codes{i,1}(2:end);


            letter_fin = codes{i,2}(1);
            number_fin = codes{i,2}(2:end);

            start = find(alf==letter_start) * 1000 + str2double(number_start);
            fin = find(alf==letter_fin) * 1000 + str2double(number_fin);

            if start<=code && code<=fin
                ch(i)=1;
                break
            end
        end
    end
end

chapter = ch;