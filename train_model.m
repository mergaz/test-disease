clc
clear
close all

load prepared_data_part_4
load output_data.mat


sample_submission = readtable('data/sample_submission.csv');
sample_submission(:,1) = input_data_2019.member_id;
hcc = output_data.Properties.VariableNames(2:end);


% Так как нужно прогнозировать только новые болезни, то если пациент уже
% болел в 2018 или 2019 годы определенным hcc, в 2020-м вероятность новой
% болезни с этим hcc автоматически приравниваем нулю
old_hcc_2018 = double(input_data_2018{:,49:77}>0);
old_hcc_2019 =  double(input_data_2019{:,49:77}>0);
old_hcc_2019 = double((old_hcc_2019==1) | (old_hcc_2018==1));

% Для быстрого старта оставим только чиловые предикторы, а пол заменим
% значеними -1 и +1
gender = input_data_2018.patient_gender;
gender_code = double(strcmp(gender,'M'));
gender_code(gender_code==0) = -1;
X = [gender_code,input_data_2018{:,[2,10:end]}];
X(isinf(X))=0;
X(isnan(X))=0;


gender = input_data_2019.patient_gender;
gender_code = double(strcmp(gender,'M'));
gender_code(gender_code==0) = -1;
Xt = [gender_code,input_data_2019{:,[2,10:end]}];
Xt(isinf(Xt))=0;
Xt(isnan(Xt))=0;


% Так как id пациентов отсортированы по алфавиту, можно не рандомизировать
% выборки, а брать перву часть для обучения, вторую для валидации
train_part = 0.85;

% Для каждого hcc - своя модель
for i = 1:length(hcc)
    y{i} = output_data{:, i + 1};
    Xx = X(old_hcc_2018(:,i)==0,:);
    len = size(Xx,1);
    len_train = round(len * train_part);
    model{i} = fitcensemble(Xx(1:len_train,:),y{i}(1:len_train),'Method','LPBoost','NumLearningCycles',20,'Learners','tree','NPrint',1);
    model{i}.ScoreTransform = 'doublelogit';
    [labels{i},scores{i}] = predict(model{i}, Xx(len_train+1:end,:));
    figure;plotconfusion(labels{i}',y{i}(len_train+1:end)');
    [labels_test,scores_test] = predict(model{i}, Xt);
    scores_test(old_hcc_2018(:,i)==1) = 0;
    sample_submission{:,i+1} = scores_test(:,2);
end



%% К сожалению, большая часть времени ушла на подготовку данных.
%% Тем не менее, это наиболее важная часть работы, так как всё остальное - выбор модели и настройка гиперпараметров - хорошо алгоритмизировано и требует просто времени.
%% Вместо нескольких тысяч предикторов мы получили менее 100, что позволяет не очень времяёмкими процедурами подобрать модель.
%% Выбрав модель (это вполне может быть и LPBoost), нужно будет добиться хорошей обобщающей способности, а затем при необходимости регуляризовать.
%% Для повышения обобщающей способности нужно будет создать сетку гиперпараметров, сохранить результаты каждого запуска, а затем с помощью простой регрессии найти оптимум.
%% Помимо подбора гиперпараметров и параметров регуляризации можно попытаться улучшить резульат добавлением новых данных.
%% Например, ввести в рассмотрение больше анализов и больше лекарств, вернуть категорийые параметры из данных пациентов (город, компания).
%% Стоит посоветоваться со специалистами по поводу функции влияния времени - нужна ли она, или какого вида одна должна быть.
%% Может оказаться, что деленеи всех болезней на 22 группы - слишком сильное ужатие днных, и вместо этого стоит по той же классификации выделить авжные подгруппы.
%% Всё это - неплохой план работы на несколько недель коллективу из 2-3 человек.
